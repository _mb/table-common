CREATE TABLE users (
                                    id bigserial PRIMARY KEY,
                                    name varchar(100) NOT NULL,
                                    surname varchar(100) NOT NULL,
                                    email varchar(100) NOT NULL,
                                    phone varchar(100) NOT NULL,
                                    created timestamp without time zone,
                                    last_updated timestamp without time zone
);
