package test.task.tablecommon.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import test.task.tablecommon.dto.RecordDto;

@Controller
public class RecordController {

    //https://spring.io/guides/gs/messaging-stomp-websocket/
    // todo rest + WS
    @MessageMapping("/hello")
    @SendTo("/topic/records")
    public RecordDto sendUpdates(RecordDto message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new RecordDto();
    }

    @PostMapping
    public ResponseEntity<?> create() {
        throw new UnsupportedOperationException();
    }

    @DeleteMapping
    public ResponseEntity<?> delete() {
        throw new UnsupportedOperationException();
    }

    @PutMapping
    public ResponseEntity<?> update() {
        throw new UnsupportedOperationException();
    }

    @GetMapping
    public ResponseEntity<?> retrieveOne() {
        throw new UnsupportedOperationException();
    }

    @GetMapping
    public ResponseEntity<?> retrieveAll() {
        throw new UnsupportedOperationException();
    }
}
