package test.task.tablecommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TableCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(TableCommonApplication.class, args);
	}

}
