package test.task.tablecommon.entity;

import lombok.val;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;
    @Column
    private String surname;
    @Column
    private String email;
    @Column
    private String phone;

    @Column(name = "created")
    private LocalDateTime createdTime;
    @Column(name = "last_updated")
    private LocalDateTime lastUpdatedTime;

    @PreUpdate
    public void onUpdate() {
        this.lastUpdatedTime = LocalDateTime.now();
    }

    @PrePersist
    public void onCreate() {
        val now = LocalDateTime.now();
        this.createdTime = now;
        this.lastUpdatedTime = now;
    }
}
